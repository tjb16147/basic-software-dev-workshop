import pandas as pd
from sklearn import model_selection
from apyori import apriori

#import csv file

def load_data(filename):
  data = pd.read_csv('store_data.csv')
  #print (data)  #print 7.5k row, 20 col >> for space, pandas fill out with a NaN
  return data


def preprocess(data):
  item_list = data.unstack().dropna().unique()  #deframe,data frame , unique >> group by change into set  >> no duplicate
  #print(item_list)
  #print(' # items:', len(item_list))

  train, test = model_selection.train_test_split(data, test_size=.10)  # test_size .10 = 10% , use data not item_list)
  #print("train: ", train)
  #print("test", test)

  train = train.T.apply(lambda x: x.dropna().tolist()).tolist()   #Transpose  drop-na
  #print("Transforms (Transpose): ", train)
  print("Transforms: ")

  return train, test

#for i in train[:10]:
  #print(i)



def model(train_data, min_support =0.0045, min_confidence=0.2, min_lift=3, min_length=2):
  result = list(apriori(train_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
  #print("result")
  #print(result)


  #for rule in result:  ##print result in form of each array
  #  print(rule)
  #  print('--------------------')
  return result



def visualize(result):
  for rule in result:
    pair = rule[0]
    components = [i for i in pair]
    print(pair)
    print('Rule: ', components[0], '->', components[1])
    print('Support: ', rule[1])
    print('Confidence:', rule[2][0][2])
    print('Lift: ', rule[2][0][3])
    print("---------------------------------------")


#item_list = data.join


if __name__ == '__main__':
#  print('Hello world')

  data = load_data('store_data.csv')
  #data2 = load_data('store2_data.csv')
  train, test = preprocess(data)
  md = model(train)
  visualize(md)